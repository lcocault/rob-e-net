# Scope

The Rob(e)Net project is a pure analogic electronic device in charge of
comparing a temperature to two thresholds and to switch on a LED depending on
the range of the temperature measured:

* Blue LED when the temperature is "cold"

* Green LED when the temperature is "tempered"

* Red LED when the temperature is "hot"

# Design

The device is based on two operational amplifiers, one NPN transistor and
a temperature sensor (represented by a variable resistor) as shown in the
following diagram.

![Principle](res/principle.png)

There are three main states for the device:

* When the temperature is low, the voltage at the output of the temperature
sensor is also low. This voltage is applied to both the "-" of the upper OpAmp
and the "+" of the lower OpAmp. Since the voltage is low, only the upper
OpAmp is active: the blue LED is "on" and the red LED is "off". Since there
is a high level applied to the base of the NPN transistor, the current is drawn
from the collector to the emitter and no current is passing through the green
LED which remains "off".

* When the temperatre is high, the situation is the exact opposite for the red and blue LED: only the lower OpAmp is active; the red LED is "on" and the blue
one is "off". The green LED is also "off" since there is still a sufficient voltage
applied to the NPN transistor base.

* When the temperature is average, none of the OpAmp is active. The red and
blue LEDs are consequently "off" and there is a low voltage applied to the
NPN transistor that does not draw the current. The current therefore passes
through the green LED which is "on".

The [Falstad](fal/principle.txt) canbe loaded to illustrate this dynamics.


# Bread-Board Prototype

The prototype is based on the following components (for a total amount of
8 euros) :

* 2 | CR2032 Lithium Battery Support | 1,10 euros each

* 2 | CR2032 Lithium Battery 3V | 1,00 euro each

* 1 | Operational Amplifier LM358N | 0,21 €

* 1 | Transistor 2N3904, NPN 40 V 200 mA, TO-92  | 0,72 €

* 1 | LM35DZ temperature sensor  | 2,10 €

* 3 | LED (red, green, yellow... instead of blue) | 0,25 € euros each

* 4 | 220 ohm resistor | 0,01 € each

* 2 | 100 ohm resistor | 0,01 € each

* 1 | 4700 ohm resistor | 0,01 €

* 1 | 1000 ohm resistor | 0,01 €

The bread-board prototype is illustrated in the following picture :

![Bread-Board Prototype](res/bread-board-prototype.png)


# PCB Design

To be documented

# PCB Realisation

To be documented

# Packaging Design

To be documented

# Packaging Realisation

To be documented

#License

All the elements of the project are licensed by Laurent COCAULT under the
Apache License Version 2.0. A copy of this license is provided in the
[LICENSE.txt](LICENSE.txt) file.


# Content

The fal directory contains the [Falstad](http://www.falstad.com/circuit-java/)
files.
The fzz directory contains the [Fritzing](http://fritzing.org/home/) files.
